<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="Css/Css.css">

    <title>Formulaire</title>
</head>

<body>
    <div class="container">
        <div class="col-md-offset-3 col-md-6">
            <div class="row">
                <div class="entete">
                    - INSCRIPTION -
                </div>
            </div>
            <div class="row">
                <div class="corps">
                    <form action="traitement.php" method="POST">
                        <div class="input-group style_1">
                            <span class="input-group-addon glyphicon glyphicon-user style_2"></span>
                            <input type="text" class="form-control" id="nom" name="nom" pattern="[A-Z]{2,30}" title="Les noms d'utilisateurs doivent être en majuscules et contenir 2 à 8 caractères." placeholder="Entrez votre Prenom" placeholder="Votre Nom" required="">
                        </div>

                        <div class="input-group style_1">
                            <span class="input-group-addon glyphicon glyphicon-user style_2"></span>
                            <input type="text" class="form-control" id="prenom" name="prenom" pattern="[a-z]{2,30}" title="Les prenoms d'utilisateurs doivent être en minuscules et contenir 2 à 8 caractères." placeholder="Entrez votre Prenom" required="">
                        </div>
                        <div class="input-group style_1">
                            <span class="input-group-addon style_2">Ø</span>
                            <select name="sexe" id="select" class="form-control">
                                <option selected="selected" disabled="">Choix Sexe...</option>
                                <option> HOMME</option>
                                <option> FEMME</option>
                            </select>

                        </div>

                        <div class="input-group style_1">
                            <span class="input-group-addon glyphicon glyphicon-globe style_2"></span>
                            <select name="pays" id="select" class="form-control">
                                <option selected="selected" disabled="">Choix du Pays...</option>
                                <option value=""> Belize <span>+501</span></option>
                                <option value=""> Benin <span>+229</span></option>
                                <option value=""> Bhutan <span>+975</span></option>
                                <option value=""> Bolivia <span>+591</span></option>
                                <option value=""> Bonaire, Sint Eustatius <span>+599</span></option>
                                <option value=""> Bosnia and Herzegovina <span>+387</span></option>
                                <option value=""> Botswana <span>+267</span></option>
                                <option value=""> Brazil <span>+55</span></option>
                                <option value=""> Brunei <span>+673</span></option>
                                <option value=""> Bulgaria <span>+359</span></option>
                                <option value=""> Burkina Faso <span>+226</span></option>
                                <option value=""> Burundi <span>+257</span></option>
                                <option value=""> Cambodia <span>+855</span></option>
                                <option value=""> Cameroon <span>+237</span></option>
                                <option value=""> Canada <span>+1</span></option>
                                <option value=""> Cape Verde <span>+238</span>
                                <option value=""> Cayman Islands <span>+1345</span>
                                <option value=""> Central African Republic <span>+236</span></option>
                                <option value=""> Chad <span>+235</span></option>
                                <option value=""> Chile <span>+56</span></option>
                                <option value=""> China <span>+86</span></option>
                                <option value=""> Colombia <span>+57</span></option>
                                <option value=""> Comoros and Mayotte <span>+269</span></option>
                                <option value=""> Congo <span>+242</span></option>
                                <option value=""> Congo Dem Rep <span>+243</span></option>
                                <option value=""> Cook Islands <span>+682</span></option>
                                <option value=""> Costa Rica <span>+506</span></option>
                                <option value=""> Cote d'Ivoire <span>+225</span></option>
                                <option value=""> Croatia <span>+385</span></option>
                                <option value=""> Cuba <span>+53</span></option>
                                <option value=""> Curaçao <span>+599</span></option>
                                <option value=""> Cyprus <span>+357</span></option>
                                <option value=""> Czech Republic <span>+420</span></option>
                                <option value=""> Denmark <span>+45</span></option>
                                <option value=""> Diego Garcia <span>+246</span></option>
                                <option value=""> Djibouti <span>+253</span></option>
                                <option value=""> Ecuador <span>+593</span></option>
                                <option value=""> Egypt <span>+20</span></option>
                                <option value=""> El Salvador <span>+503</span></option>
                                <option value=""> Equatorial Guinea <span>+240</span></option>
                                <option value=""> Eritrea <span>+291</span></option>
                                <option value=""> Estonia <span>+372</span></option>
                                <option value=""> Ethiopia <span>+251</span></option>
                                <option value=""> Falkland Islands <span>+500</span></option>
                                <option value=""> Faroe Islands <span>+298</span></option>
                                <option value=""> Fiji <span>+679</span></option>
                                <option value=""> Finland <span>+358</span></option>
                                <option value=""> France <span>+33</span></option>
                                <option value=""> French Guiana <span>+594</span></option>
                                <option value=""> French Polynesia <span>+689</span></option>
                                <option value=""> Gabon <span>+241</span></option>
                                <option value=""> Gambia <span>+220</span></option>
                                <option value=""> Georgia <span>+995</span></option>
                                <option value=""> Kuwait<span>+965</span></option>
                                <option value=""> Kyrgyzstan<span>+996</span></option>
                                <option value=""> Laos<span>+856</span></option>
                                <option value=""> Latvia<span>+371</span></option>
                                <option value=""> Lebanon<span>+961</span></option>
                                <option value=""> Lesotho<span>+266</span></option>
                                <option value=""> Liberia<span>+231</span></option>
                                <option value=""> Libya<span>+218</span></option>
                                <option value=""> Marshall Islands <span>+692</span></option>
                                <option value=""> Martinique <span>+596</span></option>
                                <option value=""> Mauritania <span>+222</span></option>
                                <option value=""> Mexico <span>+52</span></option>
                                <option value=""> Micronesia <span>+691</span></option>
                                <option value=""> Moldova <span>+373</span></option>
                                <option value=""> Monaco <span>+377</span></option>
                                <option value=""> Mongolia <span>+976</span></option>
                                <option value=""> Montenegro <span>+382</span></option>
                                <option value=""> Montserrat <span>+1664</span></option>
                                <option value=""> Morocco <span>+212</span></option>
                                <option value=""> Mozambique <span>+258</span></option>
                                <option value=""> Myanmar <span>+95</span></option>
                                <option value=""> Namibia <span>+264</span></option>
                                <option value=""> Nauru <span>+674</span></option>
                                <option value=""> Nepal <span>+977</span></option>
                                <option value=""> Netherlands <span>+31</span></option>
                                <option value=""> New Caledonia <span>+687</span></option>
                                <option value=""> New Zealand <span>+64</span></option>
                                <option value=""> Nicaragua <span>+505</span></option>
                                <option value=""> Niger <span>+227</span></option>
                                <option value=""> Nigeria <span>+234</span></option>
                                <option value=""> Niue <span>+683</span></option>
                                <option value=""> Norfolk Island <span>+6723</span></option>
                                <option value=""> Philippines <span>+63</span></option>
                                <option value=""> Portugal <span>+351</span></option>
                                <option value=""> Puerto Rico <span>+1787</span></option>
                                <option value=""> Qatar<span>+974</span></option>
                                <option value=""> Reunion<span>+262</span></option>
                                <option value=""> Saint Vincent Grenadines<span>+1784</span></option>
                                <option value=""> Samoa<span>+685</span></option>
                                <option value=""> San Marino<span>+378</span></option>
                                <option value=""> Sao Tome and Principe<span>+239</span></option>
                                <option value=""> Saudi Arabia<span>+966</span></option>
                                <option value=""> Senegal<span>+221</span></option>
                                <option value=""> Serbia<span>+381</span></option>
                                <option value=""> Seychelles<span>+248</span></option>
                                <option value=""> Sierra Leone<span>+232</span></option>
                                <option value=""> Singapore<span>+65</span></option>
                                <option value=""> Sint Maarten<span>+1721</span></option>
                                <option value=""> Slovakia<span>+421</span></option>
                                <option value=""> Slovenia<span>+386</span></option>
                                <option value=""> Solomon Islands<span>+677</span></option>
                                <option value=""> Somalia<span>+252</span></option>
                                <option value=""> South Africa<span>+27</span></option>
                                <option value=""> South Sudan<span>+211</span></option>
                                <option value=""> Spain<span>+34</span></option>
                                <option value=""> Sri Lanka<span>+94</span></option>
                                <option value=""> Sudan<span>+249</span></option>
                                <option value=""> Suriname<span>+597</span></option>
                                <option value=""> Swaziland<span>+268</span></option>
                                <option value=""> Sweden<span>+46</span></option>
                                <option value=""> Switzerland<span>+41</span></option>
                                <option value=""> Trinidad and Tobago<span>+1868</span></option>
                                <option value=""> Tunisia<span>+216</span></option>
                                <option value=""> Turkey<span>+90</span></option>
                                <option value=""> Turkmenistan<span>+993</span></option>
                                <option value=""> Turks and Caicos<span>+1649</span></option>
                                <option value=""> Tuvalu<span>+688</span></option>
                                <option value=""> Uganda<span>+256</span></option>
                                <option value=""> Ukraine<span>+380</span></option>
                                <option value=""> United Arab Emirates<span>+971</span></option>
                                <option value=""> United Kingdom<span>+44</span></option>
                                <option value=""> Venezuela<span>+58</span></option>
                                <option value=""> Vietnam<span>+84</span></option>
                                <option value=""> Virgin Islands, British<span>+1284</span></option>
                                <option value=""> Virgin Islands, US<span>+1340</span></option>
                                <option value=""> Wallis and Futuna<span>+681</span></option>
                                <option value=""> Yemen<span>+967</span></option>
                                <option value=""> Zambia<span>+260</span></option>
                                <option value=""> Zimbabwe<span>+263</span></option>
                            </select>
                        </div>

                        <div class="input-group style_1">
                            <label class="input-group-addon  glyphicon glyphicon-phone-alt style_2" for="photo"></label>
                            <input type="tel" class="form-control" id="phone" name="telephone" pattern="[0-9]{3} [0-9]{3} [0-9]{3}" title="Exemple: 999 999 999" placeholder="Entrez votre Téléphone" required="">
                        </div>

                        <div class="input-group style_1">
                            <span class="input-group-addon style_2">@</span>
                            <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Votre_adresse@exemple.com" required="">
                        </div>

                        <div class="input-group style_1">
                            <label class="input-group-addon glyphicon glyphicon-list-alt style_2"></label>
                            <input type="date" class="form-control" name="la_date" min="01-01-1990" max="31-12-2000" placeholder="Entrez fotre datte de naissance" required="">
                        </div>


                        <div class="input-group style_1">
                            <label class="input-group-addon glyphicon glyphicon-lock style_2"></label>
                            <input type="password" class="form-control" name="mot_de_passe" pattern=".{8,}" title="08 caractère au moins" placeholder="Entrez votre mot de passe 8 caractères minimum !!!" required="">
                        </div>

                        <div>
                            <div class="input-group style_1">
                                <label class="input-group-addon glyphicon glyphicon-picture style_2" for="photo"></label>
                                <input type="file" name="photo" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required="">
                            </div>
                            <div class="col-md-offset-4 col-md-4 style_1">
                                <img id="pp" style="height: 120px; width: 120px; border-radius: 20%; margin-left:150px;" />
                            </div>

                        </div>
                        <input type="submit" value="Envoyer" class="btn btn-block pied" name="">
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var loadFile = function(event) {
                var profil = document.getElementById('pp');
                profil.src = URL.createObjectURL(event.target.files[0]);
            };
        </script>
    </div>


</body>

</html>